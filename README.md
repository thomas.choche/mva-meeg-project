# MVA MEEG Project

Livrables + rapport pour le projet du cours MEEG du master MVA.

Afin de reproduire les résultats présentés dans le rapport, il suffit de faire tourner le script python "..._evaluation.py" associé. Normalement, si le repository git a été cloné, tous les paths devraient bien être définis et le script devrait tourner directement.
